#  Aula 6 – Vetores

- [Vídeo desta aula](https://youtu.be/8XH6i_T2Whc)

## 6.1 - Um nome, muitos valores
 
Como nós já vimos, para criar uma variável no bash/shell, nós escolhemos um nome e atribuímos a ele um valor. Esse valor pode mudar ao longo do tempo enquanto o script é executado, mas o nome sempre representará apenas um valor de cada vez.

```
:~$ fruta="banana"
:~$ echo $fruta
banana
:~$ fruta="laranja"
:~$ echo $fruta
laranja
```

E se nós quiséssemos representar, não uma fruta, mas um conjunto de frutas? Uma ideia seria atribuir cada uma das frutas a um nome diferente:

```
:~$ fruta01="banana"
:~$ fruta02="laranja"
:~$ fruta03="abacate"
:~$ fruta04="goiaba"
```

Apesar de ser uma solução até intuitiva, isso não seria muito prático, principalmente se quiséssemos percorrer todos os valores correspondentes ao conjunto de “frutas”. Felizmente, porém, o Bash nos permite criar variáveis onde um único nome pode apontar para diversos valores, e esse tipo de varável é chamada de **variável vetorial**, ou **array**.

## 6.2 – Criando vetores indexados

O processo de criação de vetores indexados é tão simples quanto a criação de variáveis escalares. A sintaxe geral é a seguinte:

```
nome[índice]=valor
```

Onde **nome** é o nome da array e **índice** pode ser um número inteiro positivo ou
uma string. Por exemplo:

```
fruta[0]="banana"
fruta[1]="laranja"
fruta[2]="abacate"
```

Ou ainda...

```
fruta[doce]="banana"
fruta[azeda]="laranja"
fruta[verde]="abacate"
```

Quando o índice é um número inteiro, nós chamamos esse tipo de variável **vetor
indexado**, quando for uma string, nós teremos um **vetor associativo**.

Especificamente no Bash, uma array indexada pode ser criada da seguinte forma:

```
fruta=("banana" "laranja" "abacate")
```

Onde cada valor receberá automaticamente um índice correspondente à sua ordem de ocorrência, sempre contando a partir de `0` (zero).

Finalmente, caso você queira apenas inicializar uma array antes de atribuir qualquer
valor a ela, basta utilizar uma das sintaxes abaixo:

```
# Arrays indexadas
declare -a nome_da_array

# Arrays associativas
declare -A nome_da_array
```

> Declarar arrays indexadas é opcional, mas **declarar arrays associativas é obrigatório**.

Por exemplo:

```
:~$ declare -A fruta
:~$ fruta[verde]=abacate
:~$ fruta[vermelha]=cereja
:~$ fruta[amarela]=banana
```

Se uma variável associativa não for declarada, ao acessá-la, apenas o valor do último elemento será retornado, pois toda a parte entre colchetes será ignorada no processo de atribuição dos valores.

Exemplo:

```
:~$ fruta[verde]=abacate
:~$ fruta[vermelha]=cereja
:~$ fruta[amarela]=banana
:~$ echo ${fruta[verde]}
banana
```

## 6.3 – Acessando os valores da array

O processo de leitura dos valores em uma array é um pouco diferente do que nós vimos até aqui. Por se tratar de uma identificação composta por um nome e um índice, o shell precisará de algo que indique que estamos falando de todo um conjunto de identificadores, e não apenas de um nome seguindo de uma string.

Veja o exemplo...

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo $fruta
banana
:~$ echo $fruta[2]
banana[2]
```

Invocando apenas `$fruta`, o shell retorna somente o valor correspondente ao primeiro elemento da array (`banana`). Quanto tentamos ser mais específicos fazendo referência ao índice da fruta que nos interessa, `$fruta[2]`, o retorno foi o mesmo de antes acrescido da string `[2]`.

Para que o shell entenda que estamos falando de um nome e um índice, todo esse
conjunto deve estar entre chaves, como na sintaxe geral abaixo:

```
${nome[índice]}
```

Aplicando ao exemplo...

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo ${fruta[2]}
abacate
```

## 6.4 – Lendo todos os valores de uma array

Nós podemos obter todos os valores de uma array de duas formas: como uma única string ou como strings separadas.

Para obter uma string composta de todos os elementos de uma array, nós utilizamos o caractere asterisco (`*`) no índice...

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo ${fruta[*]}
banana laranja abacate
```

Neste caso, `banana laranja abacate` formam uma única string. Se quisermos obter os mesmos valores, mas como strings separadas, nós utilizamos o caractere arroba (`@`) no índice...

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo ${fruta[@]}
banana laranja abacate
```

Embora não seja possível notar na saída do terminal, neste caso estamos diante de três strings distintas, e isso será muito útil quando precisarmos percorrer os valores de uma array, o que nós veremos mais adiante no curso, quando falarmos das estruturas de repetição.

Se, em vez dos valores, nós quisermos os índices de cada elemento, basta acrescentar o caractere de exclamação (`!`) antes do nome da array:

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo ${!fruta[@]}
0 1 2
```

## 6.5 – Listando os valores da array por faixas de índices

É possível listar os valores numa array a partir de um determinado ponto utilizando a sintaxe...

```
${nome[@]:início}
```

Onde **início** é o índice a partir do qual queremos obter a lista de valores.

Por exemplo:

```
:~$ fruta=("banana" "laranja" "abacate" "limão" "abacaxi")
:~$ echo ${fruta[@]:2}
abacate limão abacaxi
```

Também é possível obter uma faixa de valores indicando os índices de **início** e **fim**:

```
${nome[@]:início:fim}
```

Por exemplo...

```
:~$ fruta=("banana" "laranja" "abacate" "limão" "abacaxi")
:~$ echo ${fruta[@]:1:3}
laranja abacate limão
```

## 6.6 – Descobrindo o número de elementos de uma array

Nós podemos descobrir a quantidade de elementos que uma array possui, o que é feito incluindo o caractere cerquilha (`#`) antes do seu nome, por exemplo...

```
:~$ fruta[0]="banana"
:~$ fruta[1]="laranja"
:~$ fruta[2]="abacate"
:~$ echo ${#fruta[@]}
3
```
