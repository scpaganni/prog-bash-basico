# Curso Básico de Programação do Bash

## Índice

### [Aula 1 - Conceitos Básicos](aula-01.md)

- [1.1 - O que é o 'shell'](aula-01.md#1-1-o-que-%C3%A9-o-shell)
- [1.2 - Terminais e consoles](aula-01.md#1-2-terminais-e-consoles)
- [1.3 - O 'prompt' de comandos](aula-01.md#1-3-o-prompt-de-comandos)
- [1.4 - A aparência do 'prompt'](aula-01.md#1-4-a-apar%C3%AAncia-do-prompt)
- [1.5 - Shell interativo e não-interativo](aula-01.md#1-5-shell-interativo-e-n%C3%A3o-interativo)
- [1.6 - Tipos de shell](aula-01.md#1-6-tipos-de-shell)
- [1.7 - Os comandos builtin do Bash](aula-01.md#1-7-os-comandos-builtin-do-bash)
- [1.8 - Como saber que tipo de shell você está utilizando](aula-01.md#1-8-como-saber-que-tipo-de-shell-voc%C3%AA-est%C3%A1-utilizando)

### [Aula 2 - Antes do Primeiro Script](aula-02.md)

- [2.1 - Entendendo a utilidade dos scripts](aula-02.md#2-1-entendendo-a-utilidade-dos-scripts)
- [2.2 - Sobre a execução de scripts](aula-02.md#2-2-sobre-a-execu%C3%A7%C3%A3o-de-scripts)
- [2.3 - Cuidados e boas práticas](aula-02.md#2-3-cuidados-e-boas-pr%C3%A1ticas)

### [Aula 3 - Nosso Primeiro Script](aula-03.md)

- [3.1 - As etapas de criação de um script em bash](aula-03.md#3-1-as-etapas-de-cria%C3%A7%C3%A3o-de-um-script-em-bash)
- [3.2 - Etapa 1: Criar o arquivo do nosso script](aula-03.md#3-2-etapa-1-criar-o-arquivo-do-nosso-script)
- [3.3 - Etapa 2: Escrever o conteúdo do script no arquivo](aula-03.md#3-3-etapa-2-escrever-o-conte%C3%BAdo-do-script-no-arquivo)
- [3.4 - Etapa 3: Tornar o arquivo executável](aula-03.md#3-4-etapa-3-tornar-o-arquivo-execut%C3%A1vel)
- [3.5 – Enfim, nosso primeiro script!](aula-03.md#3-5-enfim-nosso-primeiro-script)
- [3.6 - Um script para criar scripts](aula-03.md#3-6-um-script-para-criar-scripts)

### [Aula 4 – Variáveis](aula-04.md)

- [4.1 - Conceito](aula-04.md#4-1-conceito)
- [4.2 - Nomeando variáveis](aula-04.md#4-2-nomeando-vari%C3%A1veis)
- [4.3 - Tipos de variáveis](aula-04.md#4-3-tipos-de-vari%C3%A1veis)
- [4.4 - Variáveis vetoriais](aula-04.md#4-4-vari%C3%A1veis-vetoriais)
- [4.5 - Variáveis inalteráveis (read-only)](aula-04.md#4-5-vari%C3%A1veis-inalter%C3%A1veis-read-only)
- [4.6 - Destruindo variáveis](aula-04.md#4-6-destruindo-vari%C3%A1veis)
- [4.7 - Atribuindo saídas de comandos a variáveis](aula-04.md#4-7-atribuindo-sa%C3%ADdas-de-comandos-a-vari%C3%A1veis)
- [4.8 - Acessando os valores das variáveis](aula-04.md#4-8-acessando-os-valores-das-vari%C3%A1veis)

### [Aula 5 – Variáveis Especiais](aula-05.md)

- [5.1 - Caracteres com significado especial](aula-05.md#5-1-caracteres-com-significado-especial)
- [5.2 - Obtendo o status de saída do último comando](aula-05.md#5-2-obtendo-o-status-de-sa%C3%ADda-do-%C3%BAltimo-comando)
- [5.3 - Passando argumentos para o scripts](aula-05.md#5-3-passando-argumentos-para-o-scripts)
- [5.4 - Contando o número de argumentos](aula-05.md#5-4-contando-o-n%C3%BAmero-de-argumentos)
- [5.5 - Um pequeno resumo](aula-05.md#5-5-um-pequeno-resumo)

### [Aula 6 – Vetores](aula-06.md)

- [6.1 - Um nome, muitos valores](aula-06.md#6-1-um-nome-muitos-valores)
- [6.2 – Criando vetores indexados](aula-06.md#6-2-criando-vetores-indexados)
- [6.3 – Acessando os valores da array](aula-06.md#6-3-acessando-os-valores-da-array)
- [6.4 – Lendo todos os valores de uma array](aula-06.md#6-4-lendo-todos-os-valores-de-uma-array)
- [6.5 – Listando os valores da array por faixas de índices](aula-06.md#6-5-listando-os-valores-da-array-por-faixas-de-%C3%ADndices)
- [6.6 – Descobrindo o número de elementos de uma array](aula-06.md#6-6-descobrindo-o-n%C3%BAmero-de-elementos-de-uma-array)

### [Aula 7 – Concatenação de Strings](aula-07.md)

- [7.1 - Expansão de parâmetros](aula-07.md#7-1-expans%C3%A3o-de-par%C3%A2metros)
- [7.2 - Inserindo strings em strings](aula-07.md#7-2-inserindo-strings-em-strings)
- [7.3 – O operador de concatenação](aula-07.md#7-3-o-operador-de-concatena%C3%A7%C3%A3o)

### [Aula 8 – Operações Aritméticas](aula-08.md)

- [8.1 - As operações básicas](aula-08.md#8-1-as-opera%C3%A7%C3%B5es-b%C3%A1sicas)
- [8.2 - Operadores aritméticos](aula-08.md#8-2-operadores-aritm%C3%A9ticos)
- [8.3 - Operadores de atribuição](aula-08.md#8-3-operadores-de-atribui%C3%A7%C3%A3o)
- [8.4 - Precedência](aula-08.md#8-4-preced%C3%AAncia)
- [8.5 – O problema do 'declare -i'](aula-08.md#8-5-o-problema-do-declare-i)
- [8.6 - O comando interno 'let'](aula-08.md#8-6-o-comando-interno-let)
- [8.7 – O comando composto '(( expressão ))'](aula-08.md#8-7-o-comando-composto-express%C3%A3o)
- [8.8 - A expansão aritmética '$(( expressão ))'](aula-08.md#8-8-a-expans%C3%A3o-aritm%C3%A9tica-express%C3%A3o)
- [8.9 - E os números não inteiros?](aula-08.md#8-9-e-os-n%C3%BAmeros-n%C3%A3o-inteiros)

### [Aula 9 – Expansões do Shell](aula-09.md)

- [9.1 - O que são expansões](aula-09.md#9-1-o-que-s%C3%A3o-expans%C3%B5es)
- [9.2 - Expansão de caminhos](aula-09.md#9-2-expans%C3%A3o-de-caminhos)
- [9.3 - Expansão de nomes de arquivos](aula-09.md#9-3-expans%C3%A3o-de-nomes-de-arquivos)
- [9.4 - Expansão de chaves](aula-09.md#9-4-expans%C3%A3o-de-chaves)
- [9.5 - Quebra (split) de palavras](aula-09.md#9-5-quebra-split-de-palavras)
- [9.6 - Substituição de comandos](aula-09.md#9-6-substitui%C3%A7%C3%A3o-de-comandos)
- [9.7 - Remoção de aspas](aula-09.md#9-7-remo%C3%A7%C3%A3o-de-aspas)

### [Aula 10 – Expansões de Parâmetros](aula-10.md)

- [10.1 - Trocando o nome pela pessoa](aula-10.md#10-1-trocando-o-nome-pela-pessoa)
- [10.2 - Indireções](aula-10.md#10-2-indire%C3%A7%C3%B5es)
- [10.3 - Substrings](aula-10.md#10-3-substrings)
- [10.4 - Comprimento de strings e número de elementos de arrays](aula-10.md#10-4-comprimento-de-strings-e-n%C3%BAmero-de-elementos-de-arrays)
- [10.5 - Testando variáveis](aula-10.md#10-5-testando-vari%C3%A1veis)
- [10.6 – Maiúsculas e minúsculas](aula-10.md#10-6-mai%C3%BAsculas-e-min%C3%BAsculas)
- [10.7 – Aparando strings](aula-10.md#10-7-aparando-strings)
- [10.8 - Busca e substituição de padrões](aula-10.md#10-8-busca-e-substitui%C3%A7%C3%A3o-de-padr%C3%B5es)

### [Aula 11 – O loop 'for'](aula-11.md)

- [11.1 - Comandos compostos](aula-11.md#11-1-comandos-compostos)
- [11.2 - Sintaxe](aula-11.md#11-2-sintaxe)
- [11.3 - Percorrendo as palavras em uma string](aula-11.md#11-3-percorrendo-as-palavras-em-uma-string)
- [11.4 - Percorrendo elementos de uma array](aula-11.md#11-4-percorrendo-elementos-de-uma-array)
- [11.5 - Percorrendo nomes de arquivos](aula-11.md#11-5-percorrendo-nomes-de-arquivos)
- [11.6 - Percorrendo faixas numéricas e alfabéticas](aula-11.md#11-6-percorrendo-faixas-num%C3%A9ricas-e-alfab%C3%A9ticas)
- [11.7 - Controlando a execução do loop 'for'](aula-11.md#11-7-controlando-a-execu%C3%A7%C3%A3o-do-loop-for)

### [Aula 12 – Loops 'while' e 'until'](aula-12.md)

- [12.1 - Estruturas de repetição condicional](aula-12.md#12-1-estruturas-de-repeti%C3%A7%C3%A3o-condicional)
- [12.2 - Sintaxe](aula-12.md#12-2-sintaxe)
- [12.3 - O loop 'while'](aula-12.md#12-3-o-loop-while)
- [12.4 - O loop 'until'](aula-12.md#12-4-o-loop-until)
- [12.5 - Loops infinitos](aula-12.md#12-5-loops-infinitos)
- [12.6 - Interrompendo loops infinitos](aula-12.md#12-6-interrompendo-loops-infinitos)

### [Aula 13 – O menu 'select'](aula-13.md)

- [13.1 - Um menu simplificado](aula-13.md#13-1-um-menu-simplificado)
- [13.2 - Sintaxe](aula-13.md#13-2-sintaxe)
- [13.3 - O prompt 'PS3'](aula-13.md#13-3-o-prompt-ps3)
- [13.4 - Menus com 'while' e 'until'](aula-13.md#13-4-menus-com-while-e-until)

### [Aula 14 – Estruturas de decisão 'if' e 'case'](aula-14.md)

- [14.1 - A estrutura 'if', 'elif', 'then', 'else'](aula-14.md#14-1-a-estrutura-if-elif-then-else)
- [14.2 - Um engano muito comum](aula-14.md#14-2-um-engano-muito-comum)
- [14.3 - Operadores de encadeamento condicional](aula-14.md#14-3-operadores-de-encadeamento-condicional)
- [14.4 – A estrutura 'case'](aula-14.md#14-4-a-estrutura-case)

### [Aula 15 – Funções](aula-15.md)

- [15.1 - Conceito](aula-15.md#15-1-conceito)
- [15.2 - Sintaxe geral](aula-15.md#15-2-sintaxe-geral)
- [15.3 - Nossa primeira função](aula-15.md#15-3-nossa-primeira-fun%C3%A7%C3%A3o)
- [15.4 - Passando "argumentos"](aula-15.md#15-4-passando-argumentos)
- [15.5 - Retornando valores](aula-15.md#15-5-retornando-valores)
- [15.6 - Escopo de variáveis](aula-15.md#15-6-escopo-de-vari%C3%A1veis)
- [15.7 - A variável 'FUNCNAME'](aula-15.md#15-7-a-vari%C3%A1vel-funcname)
- [15.8 - Diferenciando funções de comandos com o mesmo nome](aula-15.md#15-8-diferenciando-fun%C3%A7%C3%B5es-de-comandos-com-o-mesmo-nome)




