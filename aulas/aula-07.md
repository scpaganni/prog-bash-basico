#  Aula 7 – Concatenação de Strings

- [Vídeo desta aula](https://youtu.be/dF1LJLaWrms)

## 7.1 - Expansão de parâmetros

**Sob certas condições**, seja no terminal ou em scripts, quando fazemos referência a uma variável acrescentando o prefixo `$` ao seu nome, o Bash “entende” que o que nós queremos é o valor que essa variável armazena e realiza uma **substituição**. Sendo mais exato, este processo é chamado de **expansão de parâmetros**.

Observe os exemplos abaixo:

```
:~$ cor="verde"

# Sem aspas...
:~$ echo $cor
verde

# Com aspas duplas...
:~$ echo "$cor"
verde

# Com aspas simples...
:~$ echo '$cor'
$cor

# Com aspas duplas e caractere de escape...
:~$ echo "\$cor"
$cor
```

Repare que, nos dois últimos casos, o Bash não expandiu a variável e tratou `$cor` literalmente como uma string. Isso acontece porque as aspas (ou a ausência delas) e o caractere de escape (`\`) também possuem um significado especial na interpretação de comandos, o que pode ser resumido em algumas “regrinhas” simples.

| Condição | Comportamento |
|---|---|
|**Sem Aspas**| Variáveis são expandidas, mas caracteres invisíveis (quebras de linha, múltiplos espaços, tabulações, etc) são descartados e substituídos por um espaço. <br><br>No terminal, se o histórico estiver habilitado, o caractere `!` (exclamação) no início da string será interpretado como um comando de busca de eventos e executado.<br><br>É possível escapar `$`, o acento grave, `\`, aspas duplas e caracteres invisíveis.<br><br>A exclamação no início da string pode ser escapada, mas o caractere de escape também seria retornado como parte dessa string.|
|**Aspas Duplas**|Variáveis são expandidas e caracteres invisíveis são preservados.<br><br>No terminal, se o histórico estiver habilitado, o caractere `!` (exclamação) no início da string será interpretado como um comando de busca de eventos e executado.<br><br>É possível escapar `$`, o acento grave, `\`, aspas duplas e caracteres invisíveis.<br><br>A exclamação no início da string pode ser escapada, mas o caractere de escape também seria retornado como parte dessa string.|
|**Aspas Simples**|Variáveis não são expandidas, caracteres invisíveis não são preservados, e todos os caracteres especiais, inclusive a exclamação no início da string, são tratados como caracteres literais.<br><br>As aspas simples não podem ocorrer dentro de aspas simples, pois não haveria como escapá-las.|
|**Escape**|O caractere de escape (`\`) faz com que os caracteres com significado especial para o Bash sejam interpretados literalmente, a não ser quando ocorre dentro de aspas simples ou ele mesmo é escapado.|

## 7.2 - Inserindo strings em strings

Uma das formas mais comuns de concatenação, é quando nós utilizamos as variáveis para reservar o espaço (um *placeholder*) no local onde uma informação deve ser inserida.

Veja o exemplo:

```
:~$ objeto="celular"
:~$ echo "Maria tinha um $objeto."
Maria tinha um celular.

:~$ objeto="lápis"
:~$ echo "Maria tinha um $objeto."
Maria tinha um lápis.

:~$ objeto="carneirinho"
:~$ echo "Maria tinha um $objeto."
Maria tinha um carneirinho.
```

Nos três casos, a variável referenciada (`$objeto`) reservou o espaço onde os valores `celular`, `lápis` e `carneirinho` deveriam ser inseridos.

Veja outro exemplo, agora em um script que nós vamos chamar de `piada-ruim`:

```
#!/usr/bin/env bash

ben=10
leonardo=20
joaosinho=30

# Observe o caractere de escape (\) usado para impedir que a
# quebra de linha seja interpretada pelo shell!

frase="No rateio da conta, o Leonardo dá $leonardo, \
o Ben $ben, e o Joãosinho $joaosinho."

echo $frase
```

Quando executado, você veria isso no terminal:

```
:~$ ./piada-ruim
No rateio da conta, o Leonardo dá 20, o Ben 10, e o Joãosinho 30.
```

## 7.3 – O operador de concatenação

De forma geral, como vimos até aqui, não precisamos de nenhum operador para concatenar strings. Mas, em muitos casos, pode ser necessário anexar valores **ao final** do conteúdo de uma variável. Observe a seguinte situação:

```
:~$ fruta="banana"
:~$ cor="verde"
:~$ echo $fruta $cor
banana verde
```

Veja que bastou utilizar as duas variáveis separadas por um espaço como parâmetro do comando `echo` para que a string `banana verde` fosse exibida no terminal.

Ocorre, porém, o caso em que nós só queremos atualizar o valor armazenado na variável antes de utilizarmos alguma forma de exibi-la. Isso pode ser feito recorrendo ao operador de concatenação `+=`.

Por exemplo...

```
:~$ fruta="banana"
:~$ fruta+=" verde"
:~$ echo $fruta
banana verde
:~$ fruta+=" baratinha"
:~$ echo $fruta
banana verde baratinha
```

> **Importante!** O operador `+=` só serve para fazer concatenações quando estamos trabalhando com strings! Se a variável for (de fato) numérica, ele funcionará como um **operador de incremento**!

Exemplo:

```
# String...
:~$ valor=1; valor+=1
:~$ echo $valor
11

# Número inteiro...
:~$ declare -i valor; valor=1; valor+=1
:~$ echo $valor
2
```